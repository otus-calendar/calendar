FROM golang:1.13-alpine as builder
COPY . /build
WORKDIR /build
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 \
  go build -ldflags="-w -s" -mod vendor -o /go/bin/calendar

FROM alpine:3.10
COPY --from=builder /go/bin/calendar /go/bin/calendar
COPY ./scripts/run.sh /go/bin/
COPY ./scripts/wait-for.sh /go/bin
EXPOSE 8080
USER guest
WORKDIR /go/bin
CMD [ "sh", "run.sh" ]
