# Calendar

## Задание
### 2.7
Сгенерировать схему/код для работы с событиями календаря: 
- название
- начало
- конец
- тип события - enum: {встреча, напоминание, другое}

#### Что сделано
 
В модуле `.api.calendar.v1` описаны тип `Event` и простой сервис для работы с эвентами (CR~~U~~D).
Дополнительно прикручен grpc-api gateway для реализации RESTful API для работы с сервисом.
Также генерируется swagger-схема.
 
Для генерации файлов можно выполнить команду
 ```
make protos
```

а можно и не выполнять - сгенерированные файлы закоммичены в репозиторий.

Для методов реализованы заглушки в модуле `.service.calendar`

Запустить сервис можно командой
```
make run  # go run main.go
```

REST-сервис поднимается на порту 8080, доступны маршруты
- POST /v1/events
- PUT /v1/events/:id
- GET /v1/events/:id # id - uint64
- DELETE /v1/events/:id

#### Update

Добавлено простое in memory хранилище на базе мапы.


### 3.4

Создать GRPC спецификацию для сервиса-календаря из дз 2.7.
Сгенерировать GRPC сервер и клиент, проверить работу сервиса.
Отделить модель данных от Protobuf структур.

#### Что сделано

Так получилось, что grpc сервис я сделал еще в прошлый раз.

В этом ДЗ заметно поменял структуру приложения - отделил слой с календарем,
он теперь не использует ничего из protobuf/grpc, получилась просто какая-то
бизнес логика в вакууме, которая используется в grpc.

Не очень нравится преобразовывать в хэндлерах типы из прото в обычные и обратно,
получается много грязи


### 3.8 и 4.2

* Работа с очередями
  * Реализовать "напоминания" о событиях с помощью RabbitMQ.
  * Создать процесс, который периодически сканирует основную базу данных, выбирая события о которых нужно напомнить.
  * Создать процесс, который читает сообщения из очереди и шлет уведомления.

* Доработать дз 3.8, разделив код на отдельные сервисы
  * API
  * Обработчик фоновых задач
  * Отправщик уведомлений

#### Что сделано.

Реализовано три сервиса.
