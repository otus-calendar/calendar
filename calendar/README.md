# Calendar

Высокоуровневый компонент, содержит описание и реализацию бизнес логики, объявления
сущностей (entities) и интерфейса для репозиториев.

Бизнес-логика в вакууме, ничего не знает про внешние слои приложения.
