package calendar

import (
	"context"
)

// Interface describes service methods (use-cases)
type Interface interface {
	CreateEvent(ctx context.Context, event *Event) (*Event, error)
	ReadEvent(ctx context.Context, id uint64) (*Event, error)
	UpdateEvent(ctx context.Context, id uint64, event *Event) error
	DeleteEvent(ctx context.Context, id uint64) error
}

// Calendar - repository-agnostic service implementation
type Calendar struct {
	Repository Repository
}

// CreateEvent - creates new event
func (c *Calendar) CreateEvent(ctx context.Context, event *Event) (*Event, error) {
	id, err := c.Repository.Create(ctx, event)
	if err != nil {
		return nil, err
	}
	event.ID = id
	return event, nil
}

// ReadEvent - fetch event from storage by id
func (c *Calendar) ReadEvent(ctx context.Context, id uint64) (*Event, error) {
	return c.Repository.Read(ctx, id)
}

// UpdateEvent - update record in storage
func (c *Calendar) UpdateEvent(ctx context.Context, id uint64, event *Event) error {
	return c.Repository.Update(ctx, id, event)
}

// DeleteEvent - delete event from storage
func (c *Calendar) DeleteEvent(ctx context.Context, id uint64) error {
	return c.Repository.Delete(ctx, id)
}

// NewCalendar - service constructor
func NewCalendar(repository Repository) Calendar {
	return Calendar{
		Repository: repository,
	}
}
