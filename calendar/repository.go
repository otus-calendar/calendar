// describe common repository interface

package calendar

import (
	"context"
	"errors"
)

var (
	// ErrRecordNotFound - used by Read and Update methods
	ErrRecordNotFound = errors.New("record not found")
	// ErrUnableToInsertRecord - used by Create method
	ErrUnableToInsertRecord = errors.New("unable to insert record")
	// ErrUnableToUpdateRecord - used by Update method
	ErrUnableToUpdateRecord = errors.New("unable to update record")
	// ErrUnableToDeleteRecord - used by Delete method
	ErrUnableToDeleteRecord = errors.New("unable to delete record")
)

// Repository - common interface
type Repository interface {
	Create(context.Context, *Event) (uint64, error)
	Read(context.Context, uint64) (*Event, error)
	Update(context.Context, uint64, *Event) error
	Delete(context.Context, uint64) error
}
