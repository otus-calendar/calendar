// реализация сервиса на grpc

package grpc

import (
	"github.com/golang/protobuf/ptypes"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"context"

	"calendar/calendar"
)

// Calendar - struct for grpc server
type Calendar struct {
	Calendar calendar.Calendar
}

// ToCoreEvent - converts proto event into core event
func (m *Event) ToCoreEvent() (*calendar.Event, error) {
	start, err := ptypes.Timestamp(m.Start)
	if err != nil {
		return nil, err
	}
	end, err := ptypes.Timestamp(m.End)
	if err != nil {
		return nil, err
	}
	return &calendar.Event{
		ID:    m.Id,
		Name:  m.Name,
		Start: &start,
		End:   &end,
		Type:  calendar.EventType(m.Type),
	}, nil
}

// NewProtoEvent - creates proto event from core event
func NewProtoEvent(e *calendar.Event) (*Event, error) {
	start, err := ptypes.TimestampProto(*e.Start)
	if err != nil {
		return nil, err
	}
	end, err := ptypes.TimestampProto(*e.End)
	if err != nil {
		return nil, err
	}
	return &Event{
		Id:    e.ID,
		Name:  e.Name,
		Start: start,
		End:   end,
		Type:  Event_EventType(e.Type),
	}, nil
}

// CreateEvent - save new event into Storage
func (c Calendar) CreateEvent(ctx context.Context, req *CreateEventRequest) (*EventResponse, error) {
	event := req.Event
	coreEvent, err := event.ToCoreEvent()
	if err != nil {
		return nil, status.Errorf(codes.InvalidArgument, "%s", err)
	}
	coreEvent, err = c.Calendar.CreateEvent(ctx, coreEvent)
	if err != nil {
		return nil, err
	}
	res, err := NewProtoEvent(coreEvent)
	if err != nil {
		return nil, status.Errorf(codes.Unknown, "%s", err)
	}
	return &EventResponse{Result: res}, nil
}

// ReadEvent - return event by Id
func (c Calendar) ReadEvent(ctx context.Context, req *GetEventRequest) (*EventResponse, error) {
	value, err := c.Calendar.ReadEvent(ctx, req.Id)
	if err != nil {
		return nil, status.Errorf(codes.NotFound, "%s", err)
	}
	res, err := NewProtoEvent(value)
	if err != nil {
		return nil, status.Errorf(codes.Unknown, "%s", err)
	}
	return &EventResponse{Result: res}, nil
}

// UpdateEvent - return event by Id
func (c Calendar) UpdateEvent(ctx context.Context, req *UpdateEventRequest) (*UpdateEventResponse, error) {
	coreEvent, err := req.Event.ToCoreEvent()
	if err != nil {
		return nil, status.Errorf(codes.InvalidArgument, "%s", err)
	}
	err = c.Calendar.UpdateEvent(ctx, req.Id, coreEvent)
	if err != nil {
		return nil, status.Errorf(codes.NotFound, "%s", err)
	}
	return &UpdateEventResponse{}, nil
}

// DeleteEvent remove event from storage
func (c Calendar) DeleteEvent(ctx context.Context, req *DeleteEventRequest) (*DeleteEventResponse, error) {
	err := c.Calendar.DeleteEvent(ctx, req.Id)
	if err != nil {
		return nil, status.Errorf(codes.Unknown, "%s", err)
	}
	return &DeleteEventResponse{}, nil
}
