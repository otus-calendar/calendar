package main

import (
	"context"
	"log"
	"net"
	"net/http"

	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	_ "github.com/jackc/pgx/stdlib"
	"github.com/kelseyhightower/envconfig"
	"google.golang.org/grpc"

	"calendar/calendar"
	pb "calendar/grpc"
	"calendar/repositories"
)

// Config contains databasse connection string and other options
type Config struct {
	Database string `default:"postgres://postgres@localhost/calendar?sslmode=disable"`
}

func main() {
	// init and read config from environment
	config := &Config{}
	_ = envconfig.Process("calendar", config) // deliberately ignore error

	// start gRPC
	go func() {
		lis, err := net.Listen("tcp", "localhost:28080")
		if err != nil {
			log.Fatal("Cannot open socket")
		}

		repository, err := repositories.NewPostgresRepository(
			context.Background(), config.Database)
		if err != nil {
			log.Fatal("Cannot create repository: ", err)
		}
		defer repository.Close()
		calendarInstance := calendar.NewCalendar(repository)
		calendarService := pb.Calendar{Calendar: calendarInstance}

		grpcServer := grpc.NewServer()
		pb.RegisterCalendarServiceServer(grpcServer, calendarService)
		log.Println("Starting grpc server")
		_ = grpcServer.Serve(lis)
	}()

	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	mux := runtime.NewServeMux()
	opts := []grpc.DialOption{grpc.WithInsecure()}
	err := pb.RegisterCalendarServiceHandlerFromEndpoint(ctx, mux, ":28080", opts)
	if err != nil {
		log.Fatal("Cannot register handler")
	}
	log.Println("Starting HTTP server")
	_ = http.ListenAndServe(":8080", mux)
}
