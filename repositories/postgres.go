package repositories

import (
	"context"
	"fmt"
	"time"

	"github.com/jmoiron/sqlx"

	"calendar/calendar"
)

// PostgresRepository - pgx based data access object for events
type PostgresRepository struct {
	db *sqlx.DB
}

// Close closes connection to db
func (p *PostgresRepository) Close() error {
	return p.db.Close()
}

// NewPostgresRepository - constructor for data access object
func NewPostgresRepository(ctx context.Context, dsn string) (*PostgresRepository, error) {
	// create database object
	db, err := sqlx.ConnectContext(ctx, "pgx", dsn)

	if err != nil {
		return nil, err
	}
	// check connection to db
	err = db.Ping()
	if err != nil {
		return nil, err
	}
	return &PostgresRepository{
		db: db,
	}, nil
}

// Event - bordering type for separation of postgres
// and core Events representation... looks like overengineering
type Event struct {
	ID    uint64     `db:"event_id"`
	Name  string     `db:"name"`
	Start *time.Time `db:"start"`
	End   *time.Time `db:"end"`
	Type  uint8      `db:"type"`
}

// ToCalendarEvent convert db layer type to core type
func (e *Event) ToCalendarEvent() *calendar.Event {
	return &calendar.Event{
		ID:    e.ID,
		Name:  e.Name,
		Start: e.Start,
		End:   e.End,
		Type:  calendar.EventType(e.Type),
	}
}

// DbEventFromCoreEvent convert core type into db layer type
func DbEventFromCoreEvent(e *calendar.Event) *Event {
	return &Event{
		ID:    e.ID,
		Name:  e.Name,
		Start: e.Start,
		End:   e.End,
		Type:  uint8(e.Type),
	}
}

// Create - save new value into DB
func (p *PostgresRepository) Create(ctx context.Context, e *calendar.Event) (uint64, error) {
	// Lock and save new value
	dbItem := DbEventFromCoreEvent(e)
	query := `
	INSERT INTO "events"("name", "start", "end", "type")
	VALUES($1, $2, $3, $4) RETURNING "event_id"`
	// use Query (not Exec) for fetching Id
	err := p.db.QueryRowContext(ctx, query, dbItem.Name, dbItem.Start, dbItem.End, dbItem.Type).Scan(&dbItem.ID)
	if err != nil {
		return 0, fmt.Errorf("%w: %s", calendar.ErrUnableToInsertRecord, err)
	}
	return dbItem.ID, nil
}

// Read - return value from storage
func (p *PostgresRepository) Read(ctx context.Context, id uint64) (*calendar.Event, error) {
	// Lock and save new value
	query := `
	SELECT "event_id", "name", "start", "end", "type" from "events"
	WHERE "event_id" = $1 LIMIT 1
	`
	event := &Event{}
	err := p.db.GetContext(ctx, event, query, id)
	if err != nil {
		return nil, fmt.Errorf("%w: %s", calendar.ErrRecordNotFound, err)
	}
	calendarEvent := event.ToCalendarEvent()
	return calendarEvent, nil
}

// Update - rewrite existing value in storage
func (p *PostgresRepository) Update(ctx context.Context, id uint64, e *calendar.Event) error {
	query := `
	UPDATE "events"
	SET
		"name" = :name,
		"start" = :start,
		"end" = :end,
		"type" = :type
	WHERE "event_id" = :event_id
	`
	dbEvent := DbEventFromCoreEvent(e)
	dbEvent.ID = id
	_, err := p.db.NamedExecContext(ctx, query, dbEvent)
	if err != nil {
		return fmt.Errorf("%w: %s", calendar.ErrUnableToUpdateRecord, err)
	}
	return nil
}

// Delete - remove value from storage
func (p *PostgresRepository) Delete(ctx context.Context, id uint64) error {
	query := `DELETE FROM "events" WHERE "event_id" = $1`
	_, err := p.db.ExecContext(ctx, query, id)
	if err != nil {
		return fmt.Errorf("%w: %s", calendar.ErrUnableToDeleteRecord, err)
	}
	return err
}
